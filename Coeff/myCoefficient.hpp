#ifndef __FILE_MYCOEFFICIENT_HPP
#define __FILE_MYCOEFFICIENT_HPP


namespace ngfem
{
  
  class NGS_DLL_HEADER LegendreCoefficientFunction : public CoefficientFunction
  {
    int l;
  public:
    ///
    LegendreCoefficientFunction (int an) : CoefficientFunction(1), l(an) {;}
    ///
    virtual ~LegendreCoefficientFunction () {}
    ///
    virtual double Evaluate (const BaseMappedIntegrationPoint & ip) const override {       
       
	 
	double x = ip.GetPoint()(0);
	double y = ip.GetPoint()(1);
	//double theta = M_PI - atan2(y,x);
	//double xx = cos(theta);
	double xx = y;

     
	if (l == 0)
	  return 1.0;
	else if (l == 1)
	  return xx;
	else
	  {
	  double Pnm2 = 1;
	  double Pnm1 = xx;
	  double Pn;
	  for (int n = 2; n <= l; n++) {
	    Pn = double(2*n-1)/n * xx * Pnm1 - double(n-1)/n * Pnm2;
	    Pnm2 = Pnm1;
	    Pnm1 = Pn;
	  }
	  return Pn;
	}
    }  
   virtual double EvaluateConst () const {
      throw Exception("not constant");
   }
   virtual void PrintReport (ostream & ost) const {
    ost << "LegendreCoefficientFunction of order "  << l << endl;
  }
  };

  void ExportLegendre(py::module m);


  class NGS_DLL_HEADER AssocLegendreCoefficientFunction : public CoefficientFunction

  {
  
   private:
   int factorial(int n) const
   {
     return (n == 1 || n == 0) ? 1 : factorial(n-1) * n;
   }
  public: 
    int l;
    int m;
   ///
    AssocLegendreCoefficientFunction (int al,int am) : CoefficientFunction(1), l(al), m(am) {;}
    //
    virtual ~AssocLegendreCoefficientFunction () {}
    ///
    virtual double Evaluate (const BaseMappedIntegrationPoint & ip) const override {       
       
	 
	double x = ip.GetPoint()(0);
	double y = ip.GetPoint()(1);
	//double theta = M_PI - atan2(y,x);
	//double xx = cos(theta);
	double xx = y;

	if ( l < 0 || m < 0 || m > l) {
	  throw Exception("Invalid input values of l and m. Only 0 <= m <= l allowed!");
	}

        // https://en.cppreference.com/w/cpp/numeric/special_functions/assoc_legendre
	return pow(-1,m)*assoc_legendre(l,m,xx);
	// multiply with Condon–Shortley phase factor	
    }  
   virtual double EvaluateConst () const {
      throw Exception("not constant");
   }
   virtual void PrintReport (ostream & ost) const {
    ost << "AssocLegendreCoefficientFunction of degree (ell) = "  << l << ", and order (m) = " << m << endl;
  }
 };

  void ExportAssocLegendre(py::module m);


};

#endif //  __FILE_MYCOEFFICIENT_HPP
