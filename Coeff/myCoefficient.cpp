
#include <fem.hpp>
//#include <comp.hpp>
#include <python_ngstd.hpp>
#include "myCoefficient.hpp"

namespace ngfem
{
  // Export cf to Python
 
  void ExportLegendre(py::module m)
  {
    m.def("Legendre", [](int n) -> shared_ptr<CoefficientFunction>
    {
     return shared_ptr<CoefficientFunction>(make_shared<LegendreCoefficientFunction> (n));
    });
  }
  
  void ExportAssocLegendre(py::module m)
  {
    m.def("AssocLegendre", [](int l,int m) -> shared_ptr<CoefficientFunction>
    {
     return shared_ptr<CoefficientFunction>(make_shared<AssocLegendreCoefficientFunction> (l,m));
    });
  }
  
  // Register cf for pickling/archiving
  // Create static object with template parameter function and base class.
  //static RegisterClassForArchive< LegendreCoefficientFunction , CoefficientFunction> regLegendre;
  //static RegisterClassForArchive< AssocLegendreCoefficientFunction , CoefficientFunction> regAssocLegendre;
}
