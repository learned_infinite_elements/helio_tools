#include <solve.hpp>
using namespace ngsolve;
using namespace ngfem;
#include <python_comp.hpp>
#include "Coeff/myCoefficient.hpp"

PYBIND11_MODULE(helio_tools,m) {
  // import ngsolve such that python base classes are defined
  auto ngs = py::module::import("ngsolve");
 
  static size_t global_heapsize = 1000000;
  static LocalHeap glh(global_heapsize, "helio_tools", true);
  
  ExportLegendre(m);
  ExportAssocLegendre(m);

  // This adds documented flags to the docstring of objects.
  ngs.attr("_add_flags_doc")(m);
}

